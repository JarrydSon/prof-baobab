# Prof. Baobab Discord Bot #

This is the repository for the Prof. Baobab discord bot. Prof. Baobab's primary function is create leaderboards for various Pokemon Go player medals and XP. 

## Requirements ##
You will need to install [Node.js](https://nodejs.org/en/).
Additionally you need to install the following npm packages:

* [discord.js](https://discord.js.org/#/) `npm install discord.js` . 
* [enmap](https://www.npmjs.com/package/enmap) `npm install enmap`.
* [enmap-sqlite](https://www.npmjs.com/package/enmap-sqlite) `npm install enmap-sqlite`.
* [numeral](https://www.npmjs.com/package/numeral) `npm install numeral`.

## Commands ##
All commands are located in the [commands](./commands) folder. Each command must be in a separate .js file. The [index.js](./index.js) automatically handles new commands, provided they follow the specified format.

### Existing Commands ###
Arguments with < > are required. Arguments with \[ ] are optional.

Command                  | Requirements        | Function 
------------------------ |        :---:        | -------- 
`!help [command]`        | -                   | DM's the author with the bot/command help menu 
`!attack <@user_to_slap>`| -                   | Sends a message that the author used a move against `@user_to_slap`. 70% chance the move is not effective. 30% chance the move is super effective
`!leaderboard <value>`   | leaderboard channel | Replace 'leaderboard' with a valid category. Updates users value for the specified leaderboard.
`!show`                  | leaderboard channel | Sends the current leader board to the channel.
`!status`                | leaderboard channel | Shows the users ranking, the player ahead of them and by how much

## Config ##
You need to create your own config.json file that should remain private to you as it will contain the bot's login token and should not be shared publically. The file should have the following format:
```
    {
        "prefix": "<prefix of choice>",
		"token": "<your bot token>"
    }
```

## Discord.js Help ##
Checkout these brilliant Discord.js guides [guide1](https://discordjs.guide), [guide2](https://anidiotsguide_old.gitbooks.io/discord-js-bot-guide/content/) for tips on making your own bot. Most of this code is based on these.
You can find the documentation for discord.js [here](https://discord.js.org)

## Hosting a bot ##
* [guide2](https://anidiotsguide_old.gitbooks.io/discord-js-bot-guide/content/) has some hosting information.
* You can also use cloud services to host bots. My current choice is Google Cloud Platform's ['always-free'](https://cloud.google.com/free/) tier.
    * for more info on this you can checkout [this blog post](https://medium.com/google-cloud/skys-the-limit-google-cloud-platform-9dd7fa3354e4)
