const { prefix } = require('../config.json');

module.exports = {
    name: 'attack',
    description: 'Attacks another member. 30% chance that it is super effective! 70% chance that it is not very effective...',
    args: true,
    aliases: ['slap', 'salt', 'tackle', 'lick', 'peck', 'ember', 'spark', 'hex' , 'demerit'],
    usage: "(Replace 'attack' with one of the aliases) @member_to_slap",
    guildOnly: true,
    execute(message, args) {
        const oldArgs = message.content.slice(prefix.length).trim().split(/ +/g);
        const commandName = oldArgs.shift().toLowerCase();
		
		//Effectiveness
        let effectiveness = "";
        if(Math.random() >= 0.7){
            effectiveness = "super effective!"
        }
        else{
            effectiveness = "not very effective..."
        }
        //End Effectiveness
		
        switch(commandName){
            case 'salt':
                message.reply(`threw ${commandName} at ${message.mentions.members.first()}! It's ${effectiveness}`);
                break;
            default:
                message.reply(`used ${commandName} against ${message.mentions.members.first()}! It's ${effectiveness}`);
        }
                
     
    },
};