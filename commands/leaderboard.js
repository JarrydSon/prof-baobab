const numeral = require('numeral');
const { prefix } = require('../config.json');

module.exports = {
    name: 'leaderboard',
    description: 'Add your information to a specific leaderboard or view your current ranking on the leaderboard',
    args: true,
    usage: "<value for category>",
    aliases: ['update'],
    guildOnly: true,
    execute(message, args) {
        //need to recover the commandName from the original message. 
        //args has commandName removed at this stage.
        const oldArgs = message.content.slice(prefix.length).trim().split(/ +/g);
        const commandName = oldArgs.shift().toLowerCase();
        const key = `${message.guild.id}-${message.author.id}`;

        const channelName = message.channel.name.replace(/-/g, "");
        const dataBase = message.client.leaderBoard;
        
        //initialize new members
        if (!dataBase.has(key)){
            dataBase.set(key, {
                userID: message.author.id,
                guildID: message.guild.id,
                nickname: message.member.displayName,
                team: message.member.hoistRole.name
            });
        }
        
        //update user nickname if current nickname is different to the stored nickname
        if (dataBase.getProp(key, 'nickname') !== message.member.displayName){
            dataBase.setProp(key, 'nickname', message.member.displayName);
        }
    
        if (isInteger(args[0])) {
            const new_val = parseInt(args[0]);
            dataBase.setProp(key, channelName, new_val);
            message.reply(`your ${channelName} value has been updated to ${numeral(new_val).format('0,0')}`);
        
            const sorted = guildSorted();
            const top30 = sorted.splice(0, 30);
    
            let msg = '';
            let team_sym = '';

            for (const [index, data] of top30.entries()){
                if (data.team === 'instinct') team_sym = '[I]';
                else if (data.team === 'valor') team_sym = '[V]';
                else if (data.team === 'mystic') team_sym = '[M]';
                else team_sym = '(?)';
                msg += `${index + 1}. ${data.nickname} ${team_sym} ==> ${numeral(data[channelName]).format('0,0')}\n`;
            }
            
            message.channel.fetchPinnedMessages()
            .then(message => {
                //console.log(message.first().content);
                message.first().edit(`Current ${channelName} leaderboard:\n${msg}`);
            })
            .catch(err => {
                //console.error("error: " + err.message);
                message.channel.send(`Current ${channelName} leaderboard:\n${msg}`)
                .then(message => {
                    message.pin();
                });
            });  
        }
        else if(channelName !== commandName){
            message.reply("Not valid! Please make sure your leaderboard command corresponds to the channel you are posting in");
            return;
        }
        else{
            message.reply("Unknown error");
            return;
        }

	function guildSorted() {
	    const filtered = dataBase.filterArray(p => (p.guildID === message.guild.id) && (p[channelName] !== undefined));
	    const sorted = filtered.sort(compare);
	    return sorted;
	}

	function compare(a, b) {
	    if (a[channelName] === b[channelName]) return 0;
	    else return (a[channelName] < b[channelName]) ? 1 : -1;
	}
    },
};

//helper functions

function isInteger(value) {
    return /^\d+$/.test(value);
}

