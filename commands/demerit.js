const { prefix } = require('../config.json');
const numeral = require('numeral');
const random = require('random');

module.exports = {
    name: 'demerit',
    description: 'Give another member demerits. Demerits are randomly generated between 0 and 10.',
    args: true,
    usage: "@member_to_demerit: to give a random number of demerits\nstatus: to show your demerits\nshow: to show the demerit leaderboard ",
    guildOnly: true,
    execute(message, args) {
        const oldArgs = message.content.slice(prefix.length).trim().split(/ +/g);
        const commandName = oldArgs.shift().toLowerCase();
        const key = `${message.guild.id}-${message.author.id}`;
        const dataBase = message.client.leaderBoard;

        switch(args[0]){
            case 'status':
                try{
                    let demerits = dataBase.getProp(key, 'demerits');
                    message.reply(`Oh dear. You currently have ${demerits} demerits`);
                }
                catch(err){
                    message.reply(`congratulations you have no demerits!`);
                }
                break;
            case 'show':
                showDemerits();
                break;
            default:
                updateDemerits();
            
        }
        
	function updateDemerits(){
	    //generate random numbers from a normal dist mean = 5, std = 2
            const rand_generator = random.normal(mu = 5, sigma = 2);
            randDemerits = rand_generator();
            randDemerits = parseInt(randDemerits);
            //Clip the random numbersso they lie on the range [0,10]
            randDemerits = randDemerits < 0 ? 0 : randDemerits;
	    randDemerits = randDemerits > 10 ? 10 : randDemerits; 
            let newDemerits = 0;
            let mentionedMember = message.mentions.members.first();
            let mentionedKey = `${message.guild.id}-${mentionedMember.id}`;

            //initialize new members
            if (!dataBase.has(mentionedKey)){
                dataBase.set(mentionedKey, {
                    userID: mentionedMember.id,
                    guildID: message.guild.id,
                    nickname: mentionedMember.displayName,
                    team: mentionedMember.hoistRole.name
                });
            }
            
            //update user nickname if current nickname is different to the stored nickname
            if (dataBase.getProp(mentionedKey, 'nickname') !== mentionedMember.displayName){
                dataBase.setProp(mentionedKey, 'nickname', mentionedMember.displayName);
            }

            //console.log(mentionedKey);
            if (dataBase.hasProp(mentionedKey, 'demerits')){
                let oldDemerits = dataBase.getProp(mentionedKey, 'demerits');
                newDemerits = oldDemerits + randDemerits;
            }
            else {
                newDemerits = randDemerits;
            }

            dataBase.setProp(mentionedKey, 'demerits', newDemerits);

            message.reply(`gave ${mentionedMember} ${randDemerits} demerits.`);
        }

        function showDemerits(){
            const sorted = guildSorted();
            const top30 = sorted.splice(0, 30);
    
            let msg = '';
            let team_sym = '';

            for (const [index, data] of top30.entries()){
                if (data.team === 'instinct') team_sym = '[I]';
                else if (data.team === 'valor') team_sym = '[V]';
                else if (data.team === 'mystic') team_sym = '[M]';
                else team_sym = '(?)';
                msg += `${index + 1}. ${data.nickname} ${team_sym} ==> ${numeral(data['demerits']).format('0,0')}\n`;
            }
            message.channel.send(`Current naughty list:\n${msg}`);
        }
		
        function guildSorted() {
            const filtered = dataBase.filterArray(p => (p.guildID === message.guild.id) && (p['demerits'] !== undefined));
            const sorted = filtered.sort(compare);
            return sorted;
        }
    
        function compare(a, b) {
            if (a['demerits'] === b['demerits']) return 0;
            else return (a['demerits'] < b['demerits']) ? 1 : -1;
        }
    
                
     
    },
};
