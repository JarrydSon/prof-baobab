module.exports = {
    name: 'show',
    description: 'Shows the top 10 players on selected leaderboard',
    guildOnly: true,
    execute(message, args) {  
        message.channel.fetchPinnedMessages()
        .then(message => {
            let msg = '';
            msg = message.first().content;
            message.first().channel.send(msg);
        });
    },
};