const numeral = require('numeral');
const { prefix } = require('../config.json');
module.exports = {
    name: 'dbpurge',
    description: 'removes 0 valued properties and updates nicknames',
    usage: "",
    guildOnly: true,
    execute(message, args) {
        //need to recover the commandName from the original message. 
        //args has commandName removed at this stage.
        const oldArgs = message.content.slice(prefix.length).trim().split(/ +/g);
        const commandName = oldArgs.shift().toLowerCase();
        const key = `${message.guild.id}-${message.author.id}`;

        const channelName = message.channel.name.replace(/-/g, "");
        const dataBase = message.client.leaderBoard;
        const filtered = dataBase.filterArray(p => p.guildID === message.guild.id);

        for (const [index, data] of filtered.entries()){
            const key = `${data.guildID}-${data.userID}` ;
	    if (dataBase.getProp(key, 'nickname') !== message.member.guild.members.get(data.userID).displayName){
	        dataBase.setProp(key, 'nickname', message.member.guild.members.get(data.userID).displayName);
	    }
            dataBase.setProp(key, 'team', message.member.guild.members.get(data.userID).hoistRole.name);
        }
        const filtered_purge = dataBase.filterArray(p => p.guildID === message.guild.id);
        console.log(filtered_purge);
        message.reply('database has been updated');
        
    }
};
