const numeral = require('numeral');

module.exports = {
    name: 'status',
    description: 'Checks your status in the current leaderboard',
    guildOnly: true,
    execute(message, args) {
        const dataBase = message.client.leaderBoard
        const channelName = message.channel.name.replace(/-/g, "");
        const sorted = guildSorted();

        let msg = '';
        for (const [index, data] of sorted.entries()){
            if (data.userID === message.author.id){
                if (index !== 0) {
                    valueToGain = sorted[index-1][channelName] - data[channelName];
                    playerToBeat = sorted[index-1].nickname
                    msg = `You are currently ranked number ${index+1} out of ${sorted.length} players on this leaderboard. You need an additional ${numeral(valueToGain).format('0,0')} to beat ${playerToBeat}`;
                    break;
                }
                else if (index === 0){
                    valueAhead = data[channelName] - sorted[index+1][channelName];
                    playerChasing = sorted[index+1].nickname
                    msg = `Congratulations you are the top player in this category! Beware ${playerChasing} is ${numeral(valueAhead).format('0,0')} behind you`;
                    break;
                }
            }
        }

        message.reply(msg);

        //helper functions
        function guildSorted() {
            const filtered = dataBase.filterArray(p => (p.guildID === message.guild.id) && (p[channelName] !== undefined));
            const sorted = filtered.sort(compare);
            return sorted;
        }
    
        function compare(a, b) {
            if (a[channelName] === b[channelName]) return 0;
            else return (a[channelName] < b[channelName]) ? 1 : -1;
        }
    },
};
